FROM openjdk:17-jdk

VOLUME /tmp


COPY products-ban.jar products-ban.jar

ENTRYPOINT ["java","-jar","/products-ban.jar"]
