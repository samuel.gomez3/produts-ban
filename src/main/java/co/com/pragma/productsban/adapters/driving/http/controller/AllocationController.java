package co.com.pragma.productsban.adapters.driving.http.controller;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import co.com.pragma.productsban.adapters.driving.http.dto.response.CustomerProductsDTO;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;
import co.com.pragma.productsban.adapters.driving.http.handlers.IAllocationHandler;
import co.com.pragma.productsban.configuration.JwtUtil;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

@RestController
@RequestMapping("/asignment")
@RequiredArgsConstructor
public class AllocationController {

    private static final Logger logger = LoggerFactory.getLogger(AllocationController.class);
    private final IAllocationHandler serviceAssignment;

    @GetMapping("/{id}/productsElegible")
    public ResponseEntity<List<ProductFinancialDTO>> getElegibleProduct(@PathVariable long id,
                                                                        @RequestHeader("token") String token) {
        try {
            String rol = JwtUtil.validateToken(token).get("rol", String.class);
            if (!rol.equalsIgnoreCase("business")) {
                logger.warn("Unauthorized access attempt");
                return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<>(serviceAssignment.productsElegible(id), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error getting eligible products: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/productsElegibles")
    public ResponseEntity<List<CustomerProductsDTO>> getElegibleProduct2(@RequestHeader("token") String token) {
        try {
            String rol = JwtUtil.validateToken(token).get("rol", String.class);
            if (!rol.equalsIgnoreCase("business")) {
                logger.warn("Unauthorized access attempt");
                return new ResponseEntity<>(null, HttpStatus.UNAUTHORIZED);
            }
            return new ResponseEntity<>(serviceAssignment.productsElegibles(), HttpStatus.OK);
        } catch (Exception e) {
            logger.error("Error getting eligible products: {}", e.getMessage());
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
