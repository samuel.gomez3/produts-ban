package co.com.pragma.productsban.adapters.driven.jpa.mysql.mappers;

import java.util.List;

import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.ProductFinancialEntity;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;

public interface IAllocationEntityMapper {

	List<ProductFinancialDTO> toListDto(List<ProductFinancialEntity> entities);

	ProductFinancialDTO toDTO(ProductFinancialEntity producto);
}
