package co.com.pragma.productsban.adapters.driving.http.handlers.impl;

import java.util.List;
import org.springframework.stereotype.Service;
import co.com.pragma.productsban.adapters.driving.http.dto.response.CustomerProductsDTO;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;
import co.com.pragma.productsban.adapters.driving.http.handlers.IAllocationHandler;
import co.com.pragma.productsban.domain.api.IAllocationServicePort;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Service
@RequiredArgsConstructor
public class AllocationHandler implements IAllocationHandler {

	private static final Logger logger = LoggerFactory.getLogger(AllocationHandler.class);
	private final IAllocationServicePort allocationServicePort;

	@Override
	public List<ProductFinancialDTO> productsElegible(long customerId) {
		try {
			return allocationServicePort.productsElegible(customerId);
		} catch (Exception e) {
			logger.error("Error retrieving eligible products for customer ID {}: {}", customerId, e.getMessage());
			throw new RuntimeException("Could not retrieve eligible products");
		}
	}

	@Override
	public List<CustomerProductsDTO> productsElegibles() {
		try {
			return allocationServicePort.productsElegibles();
		} catch (Exception e) {
			logger.error("Error retrieving all eligible products: {}", e.getMessage());
			throw new RuntimeException("Could not retrieve all eligible products");
		}
	}
}
