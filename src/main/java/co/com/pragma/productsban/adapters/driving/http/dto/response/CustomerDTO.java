package co.com.pragma.productsban.adapters.driving.http.dto.response;



import lombok.Data;

@Data
public class CustomerDTO {
	private long id;
    private String fullName;
    private String country;
    private Double income;
    private String city;
    private int age;

}
