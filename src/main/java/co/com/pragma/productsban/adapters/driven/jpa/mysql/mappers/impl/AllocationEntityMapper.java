package co.com.pragma.productsban.adapters.driven.jpa.mysql.mappers.impl;

import org.springframework.stereotype.Component;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.stream.Collectors;

import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.ProductFinancialEntity;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.mappers.IAllocationEntityMapper;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;

@Component
public class AllocationEntityMapper implements IAllocationEntityMapper {

    private static final Logger logger = LoggerFactory.getLogger(AllocationEntityMapper.class);

    @Override
    public List<ProductFinancialDTO> toListDto(List<ProductFinancialEntity> entities) {
        try {
            return entities.stream().map(this::toDTO).collect(Collectors.toList());
        } catch (Exception e) {
            logger.error("Error while converting entity list to DTO list: {}", e.getMessage());
            throw new RuntimeException("Conversion error", e);
        }
    }

    @Override
    public ProductFinancialDTO toDTO(ProductFinancialEntity producto) {
        try {
            ProductFinancialDTO dto = new ProductFinancialDTO();
            dto.setCode(producto.getId());
            dto.setDescription(producto.getDescription());
            return dto;
        } catch (Exception e) {
            logger.error("Error while converting entity to DTO: {}", e.getMessage());
            throw new RuntimeException("Conversion error", e);
        }
    }
}

