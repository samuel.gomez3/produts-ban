package co.com.pragma.productsban.adapters.driven.jpa.mysql.adapter;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.logging.Logger;

import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.CustomerEntity;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.ProductFinancialEntity;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.mappers.IAllocationEntityMapper;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.repositories.ICustomerRepository;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.repositories.IProductFinancialRepository;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;
import co.com.pragma.productsban.domain.spi.IAlloationPersistencePort;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
public class AllocationMysqlAdapter implements IAlloationPersistencePort {

    private final IProductFinancialRepository financialRepository;
    private final ICustomerRepository customerRepository;
    private final IAllocationEntityMapper allocationEntityMapper;
    private static final Logger LOGGER = Logger.getLogger(AllocationMysqlAdapter.class.getName());

    @Override
    public CustomerEntity findById(long id) {
        try {
            return customerRepository.findById(id).orElseThrow(() -> new NoSuchElementException("Customer not found"));
        } catch (NoSuchElementException e) {
            LOGGER.severe("Customer not found: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ProductFinancialDTO> findAllProductFinancialDTO() {
        try {
            return allocationEntityMapper.toListDto(financialRepository.findAll());
        } catch (Exception e) {
            LOGGER.severe("Error fetching all financial products: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public List<CustomerEntity> findAllCustomerEntity() {
        try {
            return customerRepository.findAll();
        } catch (Exception e) {
            LOGGER.severe("Error fetching all customers: " + e.getMessage());
            throw e;
        }
    }

    @Override
    public List<ProductFinancialEntity> findAllProductFinancialEntity() {
        try {
            return financialRepository.findAll();
        } catch (Exception e) {
            LOGGER.severe("Error fetching all financial product entities: " + e.getMessage());
            throw e;
        }
    }
}
