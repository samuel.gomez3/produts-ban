package co.com.pragma.productsban.adapters.driving.http.dto.response;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class CustomerProductsDTO {
	private long id;
    private String fullName;
    private String country;
    private Double income;
    private String city;
    private int age;
    private List<ProductFinancialDTO> productFinancial;
}
