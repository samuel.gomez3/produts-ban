package co.com.pragma.productsban.adapters.driving.http.handlers;

import java.util.List;

import co.com.pragma.productsban.adapters.driving.http.dto.response.CustomerProductsDTO;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;

public interface IAllocationHandler {
	List<ProductFinancialDTO> productsElegible(long customerId);

	List<CustomerProductsDTO> productsElegibles();
}
