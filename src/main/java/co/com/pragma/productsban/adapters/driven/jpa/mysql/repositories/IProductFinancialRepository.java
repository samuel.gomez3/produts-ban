package co.com.pragma.productsban.adapters.driven.jpa.mysql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.ProductFinancialEntity;

public interface IProductFinancialRepository extends JpaRepository<ProductFinancialEntity, Long> {

}
