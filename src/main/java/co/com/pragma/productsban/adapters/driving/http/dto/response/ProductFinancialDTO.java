package co.com.pragma.productsban.adapters.driving.http.dto.response;

import lombok.Data;

@Data
public class ProductFinancialDTO {

	
	private long code;
	private String description;

}
