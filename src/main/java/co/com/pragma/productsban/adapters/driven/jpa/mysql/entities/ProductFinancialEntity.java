package co.com.pragma.productsban.adapters.driven.jpa.mysql.entities;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import lombok.Data;
import java.util.logging.Logger;

@Entity(name = "ProductFinancial")
@Data
public class ProductFinancialEntity {

	@Id
	private long id;
	private String description;
	private int ageMinimun;
	private double incomeMinimum;
	private String nationality;
	private static final Logger LOGGER = Logger.getLogger(ProductFinancialEntity.class.getName());

	public boolean isEligible(CustomerEntity customer) {
		try {
			boolean ageCheck = customer.getAge() >= ageMinimun;
			boolean incomeCheck = customer.getIncome() >= incomeMinimum;
			boolean nationalityCheck = nationality == null || nationality.equals(customer.getCountry());

			if (!ageCheck) {
				LOGGER.warning("Customer does not meet the minimum age requirement.");
			}
			if (!incomeCheck) {
				LOGGER.warning("Customer does not meet the minimum income requirement.");
			}
			if (!nationalityCheck) {
				LOGGER.warning("Customer does not meet the nationality requirement.");
			}

			return ageCheck && incomeCheck && nationalityCheck;
		} catch (Exception e) {
			LOGGER.severe("Error evaluating customer eligibility: " + e.getMessage());
			throw e;
		}
	}
}
