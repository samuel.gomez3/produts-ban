package co.com.pragma.productsban.adapters.driven.jpa.mysql.repositories;

import org.springframework.data.jpa.repository.JpaRepository;

import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.CustomerEntity;

public interface ICustomerRepository extends JpaRepository<CustomerEntity, Long>{

}
