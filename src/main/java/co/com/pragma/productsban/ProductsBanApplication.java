package co.com.pragma.productsban;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ProductsBanApplication {

	public static void main(String[] args) {
		SpringApplication.run(ProductsBanApplication.class, args);
	}

}
