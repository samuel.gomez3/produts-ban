package co.com.pragma.productsban.domain.useCase;

import java.util.List;
import java.util.stream.Collectors;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.CustomerEntity;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.mappers.IAllocationEntityMapper;
import co.com.pragma.productsban.adapters.driving.http.dto.response.CustomerProductsDTO;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;
import co.com.pragma.productsban.domain.api.IAllocationServicePort;
import co.com.pragma.productsban.domain.spi.IAlloationPersistencePort;
import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@RequiredArgsConstructor
public class AllocationUseCase implements IAllocationServicePort {

	private static final Logger logger = LoggerFactory.getLogger(AllocationUseCase.class);

	private final IAlloationPersistencePort alloationPersistencePort;
	private final IAllocationEntityMapper allocationEntityMapper;

	public List<ProductFinancialDTO> productsElegible(long customerId) {
		try {
			CustomerEntity customer = alloationPersistencePort.findById(customerId);
			if (customer == null) {
				logger.warn("Customer with ID {} not found", customerId);
				return List.of();
			}

			return alloationPersistencePort.findAllProductFinancialEntity().stream()
					.filter(producto -> producto.isEligible(customer)).map(allocationEntityMapper::toDTO)
					.collect(Collectors.toList());

		} catch (Exception e) {
			logger.error("Error fetching eligible products for customer with ID {}: {}", customerId, e.getMessage());
			throw new RuntimeException("Error fetching eligible products");
		}
	}

	public List<CustomerProductsDTO> productsElegibles() {
		try {
			List<CustomerEntity> customers = alloationPersistencePort.findAllCustomerEntity();
			if (customers.isEmpty()) {
				logger.warn("No customers found");
				return List.of();
			}

			return customers.stream()
					.map(c -> new CustomerProductsDTO(c.getId(), c.getFullName(), c.getCountry(), c.getIncome(),
							c.getCity(), c.getAge(),
							alloationPersistencePort.findAllProductFinancialEntity().stream()
									.filter(producto -> producto.isEligible(c)).map(allocationEntityMapper::toDTO)
									.collect(Collectors.toList())))
					.collect(Collectors.toList());

		} catch (Exception e) {
			logger.error("Error fetching all eligible products: {}", e.getMessage());
			throw new RuntimeException("Error fetching all eligible products");
		}
	}
}
