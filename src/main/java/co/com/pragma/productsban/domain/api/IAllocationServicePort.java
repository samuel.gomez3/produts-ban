package co.com.pragma.productsban.domain.api;

import java.util.List;

import co.com.pragma.productsban.adapters.driving.http.dto.response.CustomerProductsDTO;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;

public interface IAllocationServicePort {
	List<ProductFinancialDTO> productsElegible(long customerId);
	List<CustomerProductsDTO> productsElegibles();
}
