package co.com.pragma.productsban.domain.spi;

import java.util.List;

import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.CustomerEntity;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.ProductFinancialEntity;
import co.com.pragma.productsban.adapters.driving.http.dto.response.ProductFinancialDTO;

public interface IAlloationPersistencePort {

	
	CustomerEntity findById(long id);
	List<ProductFinancialDTO> findAllProductFinancialDTO();
	List<CustomerEntity> findAllCustomerEntity();
	List<ProductFinancialEntity> findAllProductFinancialEntity();
}
