package co.com.pragma.productsban.configuration;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import co.com.pragma.productsban.adapters.driven.jpa.mysql.adapter.AllocationMysqlAdapter;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.mappers.IAllocationEntityMapper;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.repositories.ICustomerRepository;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.repositories.IProductFinancialRepository;
import co.com.pragma.productsban.domain.api.IAllocationServicePort;
import co.com.pragma.productsban.domain.spi.IAlloationPersistencePort;
import co.com.pragma.productsban.domain.useCase.AllocationUseCase;
import lombok.RequiredArgsConstructor;

@Configuration
@RequiredArgsConstructor
public class BeanConfiguration {

	private final IProductFinancialRepository financialRepository;
	private final ICustomerRepository customerRepository;
	private final IAllocationEntityMapper allocationEntityMapper;

	@Bean
	public IAlloationPersistencePort alloationPersistencePort() {
		return new AllocationMysqlAdapter(financialRepository, customerRepository, allocationEntityMapper);
	}

	@Bean
	public IAllocationServicePort allocationServicePort() {
		return new AllocationUseCase(alloationPersistencePort(), allocationEntityMapper);
	}

}
