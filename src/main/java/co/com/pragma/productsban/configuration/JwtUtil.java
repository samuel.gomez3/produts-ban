package co.com.pragma.productsban.configuration;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.ExpiredJwtException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.Map;

@Service
public class JwtUtil {
	private static final Logger logger = LoggerFactory.getLogger(JwtUtil.class);

	private static String SECRET_KEY = "SamI1752";

	public static String generateToken(String username, Map<String, Object> claims) {
		return Jwts.builder().setClaims(claims).setSubject(username).setIssuedAt(new Date(System.currentTimeMillis()))
				.setExpiration(new Date(System.currentTimeMillis() + 600000)) // 10 minutes
				.signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
	}

	public static Claims validateToken(String token) {
		try {
			Claims claims = Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();

			return claims;
		} catch (ExpiredJwtException e) {
			logger.error("JWT Token has expired");
			throw e;
		} catch (Exception e) {
			logger.error("Failed to validate JWT token");
			throw e;
		}
	}
}
