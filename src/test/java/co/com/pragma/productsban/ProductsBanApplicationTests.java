package co.com.pragma.productsban;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.util.HashMap;

import java.util.Map;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import co.com.pragma.productsban.adapters.driven.jpa.mysql.adapter.AllocationMysqlAdapter;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.CustomerEntity;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.entities.ProductFinancialEntity;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.repositories.ICustomerRepository;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.repositories.IProductFinancialRepository;
import co.com.pragma.productsban.adapters.driving.http.handlers.impl.AllocationHandler;
import co.com.pragma.productsban.configuration.BCryptUtil;
import co.com.pragma.productsban.configuration.JwtUtil;

import co.com.pragma.productsban.domain.useCase.AllocationUseCase;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.JwtException;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

import co.com.pragma.productsban.domain.api.IAllocationServicePort;
import co.com.pragma.productsban.domain.spi.IAlloationPersistencePort;
import co.com.pragma.productsban.adapters.driven.jpa.mysql.mappers.IAllocationEntityMapper;


import java.util.NoSuchElementException;
import java.util.Optional;


class ProductsBanApplicationTests {
	@InjectMocks
	private AllocationUseCase serviceAssignment;

	@Mock
	private IProductFinancialRepository financialRepository;

	@Mock
	private ICustomerRepository customerRepository;

	private ProductFinancialEntity product;

	@InjectMocks
	private AllocationUseCase allocationUseCase;

	@Mock
	private IAlloationPersistencePort alloationPersistencePort;

	@Mock
	private IAllocationEntityMapper allocationEntityMapper;

	@InjectMocks
	private AllocationHandler allocationHandler;

	@Mock
	private IAllocationServicePort allocationServicePort;

	@InjectMocks
	private AllocationMysqlAdapter allocationMysqlAdapter;

	@BeforeEach
	public void init() {
		MockitoAnnotations.openMocks(this);
		product = new ProductFinancialEntity();
	}

	@Test
    public void testFindByIdThrowsException() {
        when(customerRepository.findById(anyLong())).thenReturn(Optional.empty());

        assertThrows(NoSuchElementException.class, () -> {
            allocationMysqlAdapter.findById(1L);
        });
    }

	@Test
    public void testFindAllProductFinancialDTOThrowsException() {
        when(financialRepository.findAll()).thenThrow(new RuntimeException("Database error"));

        assertThrows(RuntimeException.class, () -> {
            allocationMysqlAdapter.findAllProductFinancialDTO();
        });
    }

	@Test
    public void testFindAllCustomerEntityThrowsException() {
        when(customerRepository.findAll()).thenThrow(new RuntimeException("Database error"));

        assertThrows(RuntimeException.class, () -> {
            allocationMysqlAdapter.findAllCustomerEntity();
        });
    }

	@Test
    public void testFindAllProductFinancialEntityThrowsException() {
        when(financialRepository.findAll()).thenThrow(new RuntimeException("Database error"));

        assertThrows(RuntimeException.class, () -> {
            allocationMysqlAdapter.findAllProductFinancialEntity();
        });
    }

	@Test
	    public void testProductsElegibleFailure() {
	        when(alloationPersistencePort.findById(anyLong())).thenThrow(new RuntimeException("Database error"));

	        assertThrows(RuntimeException.class, () -> {
	            allocationUseCase.productsElegible(1L);
	        });
	    }

	@Test
	    public void testProductsElegiblesFailure() {
	        when(alloationPersistencePort.findAllCustomerEntity()).thenThrow(new RuntimeException("Database error"));

	        assertThrows(RuntimeException.class, () -> {
	            allocationUseCase.productsElegibles();
	        });
	    }

	@Test
	void testHashPassword() {
		String plainPassword = "myPassword";
		String hashedPassword = BCryptUtil.hashPassword(plainPassword);

		assertTrue(hashedPassword != null && !hashedPassword.isEmpty());
		assertFalse(plainPassword.equals(hashedPassword));
	}

	@Test
	void testCheckPass() {
		String plainPassword = "myPassword";
		String hashedPassword = BCryptUtil.hashPassword(plainPassword);
		assertTrue(BCryptUtil.checkPassword(plainPassword, hashedPassword));
		assertFalse(BCryptUtil.checkPassword("wrongPassword", hashedPassword));
	}

	@Test
	void testGenerateToken() {
		Map<String, Object> claims = new HashMap<>();
		claims.put("role", "user");
		String username = "john.doe";

		String token = JwtUtil.generateToken(username, claims);

		assertNotNull(token);
	}

	@Test
	void testValidateToken() {
		Map<String, Object> claims = new HashMap<>();
		claims.put("role", "user");
		String username = "john.doe";

		String token = JwtUtil.generateToken(username, claims);

		Claims parsedClaims = JwtUtil.validateToken(token);

		assertEquals(username, parsedClaims.getSubject());
		assertEquals("user", parsedClaims.get("role"));
	}

	@Test
	public void testValidateToken_InvalidToken() {
		String invalidToken = "invalidToken";
		assertThrows(JwtException.class, () -> JwtUtil.validateToken(invalidToken));
	}

	@Test
	public void testValidateToken_NullToken() {
		String nullToken = null;
		assertThrows(IllegalArgumentException.class, () -> JwtUtil.validateToken(nullToken));
	}

	@Test
	public void testValidateToken_EmptyToken() {
		String emptyToken = "";
		assertThrows(IllegalArgumentException.class, () -> JwtUtil.validateToken(emptyToken));
	}

	@Test
	public void testIsEligible_true() {
		CustomerEntity customer = new CustomerEntity();
		customer.setAge(30);
		customer.setIncome(60000.00);
		customer.setCountry("USA");

		product.setAgeMinimun(25);
		product.setIncomeMinimum(50000.00);
		product.setNationality("USA");

		assertTrue(product.isEligible(customer));
	}

	@Test
	public void testIsEligible_falseByAge() {
		CustomerEntity customer = new CustomerEntity();
		customer.setAge(20);
		customer.setIncome(60000.00);
		customer.setCountry("USA");

		product.setAgeMinimun(25);
		product.setIncomeMinimum(50000.00);
		product.setNationality("USA");

		assertFalse(product.isEligible(customer));
	}

	@Test
	public void testIsEligible_falseByIncome() {
		CustomerEntity customer = new CustomerEntity();
		customer.setAge(30);
		customer.setIncome(40000.00);
		customer.setCountry("USA");

		product.setAgeMinimun(25);
		product.setIncomeMinimum(50000.00);
		product.setNationality("USA");

		assertFalse(product.isEligible(customer));
	}

	@Test
	public void testIsEligible_falseByCountry() {
		CustomerEntity customer = new CustomerEntity();
		customer.setAge(30);
		customer.setIncome(60000.00);
		customer.setCountry("Canada");

		product.setAgeMinimun(25);
		product.setIncomeMinimum(50000.00);
		product.setNationality("USA");

		assertFalse(product.isEligible(customer));
	}

	@Test
	public void testIsEligible_trueNationalityNull() {
		CustomerEntity customer = new CustomerEntity();
		customer.setAge(30);
		customer.setIncome(60000.00);
		customer.setCountry("USA");

		product.setAgeMinimun(25);
		product.setIncomeMinimum(50000.00);
		product.setNationality(null);

		assertTrue(product.isEligible(customer));
	}

}
